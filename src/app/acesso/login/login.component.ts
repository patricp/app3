import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { Authentication } from './../../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() public exibirPainel: EventEmitter<string> = new EventEmitter<string>();
  public message: string;

  public formulario: FormGroup = new FormGroup({
    'email': new FormControl(null, [ Validators.required, Validators.email ]),
    'senha': new FormControl(null, [ Validators.required, Validators.minLength(6) ])
  });

  constructor(private authentication: Authentication) { }

  ngOnInit() {
    this.authentication.message.subscribe(
      (res) => { this.message = res; }
    );
  }

  public exibirPainelCadastro(): void {
    this.exibirPainel.emit('cadastro');
  }

  public autenticar(): void {
    this.authentication.autenticar(
      this.formulario.value.email,
      this.formulario.value.senha
    );
  }

}
