import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit(): void {
    const config = {
      apiKey: 'AIzaSyBVnYyZFlRFk7NFoLocy8MdZgnJfvTKPGk',
      authDomain: 'jta-instagram-clone-18b44.firebaseapp.com',
      databaseURL: 'https://jta-instagram-clone-18b44.firebaseio.com',
      projectId: 'jta-instagram-clone-18b44',
      storageBucket: 'jta-instagram-clone-18b44.appspot.com',
      messagingSenderId: '1030101421520'
    };

    firebase.initializeApp(config);
  }

}
